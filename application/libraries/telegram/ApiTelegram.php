
<?php
require 'Exception.php';



/*
created and modified by : IIM NUR DIANSYAH
bitbucket : https://bitbucket.org/iimnd/
email : iimnurdiansyah20@gmail.com
18 July 2016
*/

class ApiTelegram

	{
	const BASE_URL = 'https://api.telegram.org';
	const BOT_URL = '/bot';
	const FILE_URL = '/file';
	protected $token;
	public

	function __construct($token)
		{
		$this->token = $token;
		if (is_null($this->token)) throw new TelegramException('Required "token" key not supplied');
		$this->baseURL = self::BASE_URL . self::BOT_URL . $this->token . '/';
		$this->baseFileURL = self::BASE_URL . self::FILE_URL . self::BOT_URL . $this->token . '/';
		}

	public function getMe()
		{
		return $this->Request('getMe', array());
		}

		
		
		
		
		
	public function getUpdates($offset = null, $timeout = null, $limit = null)
		{
		$params = compact('offset', 'limit', 'timeout');
		return $this->Request('getUpdates', $params);
		}

		
		
		
		
		
	public function sendMessage($chat_id, $text, $parse_mode = null, $disable_web_page_preview = false, $reply_to_message_id = null, $reply_markup = null)
		{
		$params = compact('chat_id', 'text', 'parse_mode', 'disable_web_page_preview', 'reply_to_message_id', 'reply_markup');
		return $this->Request('sendMessage', $params);
		}

		
		
		
		
		
	public function forwardMessage($chat_id, $from_chat_id, $message_id)
		{

		// from : 266009680
		// messid: 53

		$params = compact('chat_id', 'from_chat_id', 'message_id');
		return $this->Request('forwardMessage', $params);
		}

		
		
		
		
		
	public function sendLocation($chat_id, $latitude, $longitude, $reply_to_message_id = null, $reply_markup = null)
		{
		$params = compact('chat_id', 'latitude', 'longitude', 'reply_to_message_id', 'reply_markup');
		return $this->Request('sendLocation', $params);
		}

		
		
		
		
		
	public function sendVenue($chat_id, $latitude, $longitude, $title, $address, $foursquare_id = null, $reply_to_message_id = null, $reply_markup = null)
		{
		$params = compact('chat_id', 'latitude', 'longitude', 'title', 'address', 'foursquare_id', 'reply_to_message_id', 'reply_markup');
		return $this->Request('sendVenue', $params);
		}

		
		
		
		
		
	public function sendContact($chat_id, $phone_number, $first_name, $last_name = null, $reply_to_message_id = null, $reply_markup = null)
		{
		$params = compact('chat_id', 'phone_number', 'first_name', 'last_name', 'reply_to_message_id', 'reply_markup');
		return $this->Request('sendContact', $params);
		}

		
		
		
		
		
	public function sendChatAction($chat_id, $action)
		{
		$actions = array(
			'typing',
			'upload_photo',
			'record_video',
			'upload_video',
			'record_audio',
			'upload_audio',
			'upload_document',
			'find_location',
		);
		if (isset($action) && in_array($action, $actions))
			{
			$params = compact('chat_id', 'action');
			return $this->Request('sendChatAction', $params);
			}

		throw new TelegramException('Invalid Action! Accepted value: ' . implode(', ', $actions));
		}

		
		
		
		
		
	public function getUserProfilePhotos($user_id, $offset = null, $limit = null)
		{
		$params = compact('user_id', 'offset', 'limit');
		return $this->Request('getUserProfilePhotos', $params);
		}

		
		
		
		
		
	public function getFilePath($file_id)
		{
		return $this->Request('getFile', compact('file_id'));
		}

		
		
		
		
		
	public function getFileData($path)
		{
		$url = $this->baseFileURL . $path;
		return $url;
		}

		
		
		
		
		
	public function sendSticker($chat_id, $path, $mime_type, $file_name, $caption = null, $reply_to_message_id = null, $reply_markup = null)
		{

		// sticker konsepnya mirip seperti gambar, cuma tidak bisa di klik saat di chat dan kalao sticker tidak ada frame nya

		return $this->uploadFile($chat_id, 'sendSticker', 'sticker', $path, $mime_type, $file_name, $caption);
		}

		
		
		
		
		
	public function sendImage($chat_id, $path, $mime_type, $file_name, $caption = null, $reply_to_message_id = null, $reply_markup = null)
		{
		return $this->uploadFile($chat_id, 'sendPhoto', 'photo', $path, $mime_type, $file_name, $caption);
		}

		
		
		
		
		
	public function sendSound($chat_id, $path, $mime_type, $file_name, $caption = null, $reply_to_message_id = null, $reply_markup = null)
		{
		return $this->uploadFile($chat_id, 'sendAudio', 'audio', $path, $mime_type, $file_name, $caption);
		}

		
		
		
		
		
	public function sendVideo($chat_id, $path, $mime_type, $file_name, $caption = null, $reply_to_message_id = null, $reply_markup = null)
		{
		return $this->uploadFile($chat_id, 'sendVideo', 'video', $path, $mime_type, $file_name, $caption);
		}

		
		
		
		
		
	public function sendDocument($chat_id, $path, $mime_type, $file_name, $caption = null, $reply_to_message_id = null, $reply_markup = null)
		{
		return $this->uploadFile($chat_id, 'sendDocument', 'document', $path, $mime_type, $file_name, $caption);
		}

		
		
		
		
		
	private function Request($method, $params)
		{
		return json_decode(file_get_contents($this->baseURL . $method . '?' . http_build_query($params)) , true);
		}

		
		
		
		
		
	private function uploadFile($chat_id, $method, $method_param, $path, $mime_type, $file_name, $caption)
		{
		$cfile = new CURLFile(realpath($path) , $mime_type, $file_name);
		$data = ['chat_id' => $chat_id, $method_param => $cfile, 'caption' => $caption];
		$ch = curl_init($this->baseURL . $method);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		print_r($result);
		curl_close($ch);
		}
	}

?>