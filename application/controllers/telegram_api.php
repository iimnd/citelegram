
<?php


/*
created and modified by : IIM NUR DIANSYAH
bitbucket : https://bitbucket.org/iimnd/
email : iimnurdiansyah20@gmail.com
18 July 2016
*/

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
    include_once(APPPATH . '/libraries/telegram/ApiTelegram.php');
class Telegram_api extends CI_Controller
{
    private $token = '11'; //input your token here (string)
	private $chat_id= 1; //input your chat id here (int)
    
    
    public function __construct()
    {
        parent::__construct();
		
        $this->tele = new ApiTelegram($this->token);
        
    }
    
    
    
    public function index()
    {
        $this->load->view('index');
    }
    
    
    
    public function getmyinfo()
    {
        
        
        
        try {
            $data = $this->tele->getMe();
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
        
        $this->load->view('profile', $data);
    }
    
	
	
	
	
	public function getupdate()
    {
        
        
        
        try {
            $data['update'] = $this->tele->getUpdates();
			//print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
        
        $this->load->view('getupdate', $data);
    }
	
	
	
	
	public function sendmessage()
    {
          try {
           	$text='hello world';
			
            $data = $this->tele->sendMessage($this->chat_id,$text);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function sendlocation()
    {
          try {
           	$lat=-6.420149;
			$long=106.840046;
			
            $data = $this->tele->sendLocation($this->chat_id,$lat, $long);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function sendvenue()
    {
          try {
           	$lat=-6.420149;
			$long=106.840046;
			$title='Rumahku';
			$address='parung';
            $data = $this->tele->sendVenue($this->chat_id,$lat, $long, $title, $address);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function sendcontact()
    {
          try {
           	$phone_number=6285775555885;
			$first_name='IIM';
			$last_name='Nur Diansyah';
			
            $data = $this->tele->sendContact($this->chat_id, $phone_number, $first_name, $last_name);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	public function sendaction()
    {
          try {
           	$action='typing';
			/*	'typing',
			'upload_photo',
			'record_video',
			'upload_video',
			'record_audio',
			'upload_audio',
			'upload_document',
			'find_location',*/
			
            $data = $this->tele->sendChatAction($this->chat_id, $action);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
		public function getpath()
    {
          try {
           	$file_id='BQADBQADAwADJNkMDWzjOnmxMNY8Ag';
			
            $data = $this->tele->getFilePath($file_id);
			
			$path = $data['result']['file_path'];
			echo $path;
//			print_r($data);
			die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
		public function download()
    {
          try {
           	$file_id='BQADBQADDwADUPzaD8LP10sIf2b2Ag';
			
            $data = $this->tele->getFilePath($file_id);
			
			$path = $data['result']['file_path'];
			$this->getfile($path);
			//echo $path;
           //print_r($data);
			die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function getfile($path)
    {
          try {
			  
			  $file_path= $path;
           	//$file_path='photo/file_7.jpg';
			
            $data = $this->tele->getFileData($file_path);
			
		    echo '<script type="text/javascript">   window.open("'.$data.'", "_blank"); </script>';;

			die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function sendsticker()
    {
          try {
           	$path=FCPATH.'upload/sticker/maldini.jpg';
		
			$file_name= 'maldini.jpg';
		
			$mime_type='';
			
            $data = $this->tele->sendSticker($this->chat_id,$path, $mime_type, $file_name, $caption='wee');
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
		public function sendimage()
    {
          try {
           	$path=FCPATH.'upload/image/maldini.jpg';
		
			$file_name= 'maldini.jpg';
		
			$mime_type='';
			
            $data = $this->tele->sendImage($this->chat_id,$path, $mime_type, $file_name, $caption='wee');
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function sendaudio()
    {
          try {
           	$path=FCPATH.'upload/sound/cat.mp3';
		
			$file_name= 'cat.mp3';
		
			$mime_type='';
			$caption='suara kucing';  //sepertinya untuk caption tidak berfungsi
			
            $data = $this->tele->sendSound($this->chat_id, $path, $mime_type, $file_name, $caption , $reply_to_message_id = null, $reply_markup = null);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function sendvideo()
    {
          try {
           	$path=FCPATH.'upload/video/1.avi';
		
			$file_name= '1.avi';
		
			$mime_type='';
			
			$caption = 'ini video';
			
            $data = $this->tele->sendVideo($this->chat_id,$path, $mime_type, $file_name, $caption);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
	
	
	
	
	
	public function senddocument()
    {
          try {
           	$path=FCPATH.'upload/document/a.xls';
		
			$file_name= 'a.xls';
		
			$mime_type='';
			
			$caption = 'ini laporan excel';
			
            $data = $this->tele->sendDocument($this->chat_id,$path, $mime_type, $file_name, $caption);
			print_r($data); die();
            
        }
        catch (Exception $e) {
            echo $e->getMessage();
        }
		
             
    }
    
    
    







}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */